#!/bin/bash

# XXX: this should be re-written to be part of our installer, possibly
# as a submodule of tsa-misc, but maybe not in fabric

set -e

# bash-specific
set -o pipefail

env DEBIAN_FRONTEND=noninteractive

apt install -y curl mmdebstrap dctrl-tools

unshare -m

# the parameters for the image

# where to fetch the packages from
# This is not necessary, its the default, and if you use the default
# the security mirror is setup automatically
#MIRROR="http://deb.debian.org/debian"

# If you specify a mirror manually, then you have to specify all of them
# manually.
#
# You might need to read that one again, because it is some mmdebstrap exotic
# magic: if you specify a mirror manually, then you have to specify all of them
# manually.
#
# If you change this list, then mmdebstrap doesn't touch it and you need to
# populate it with everything you want. You cannot just append to the mirror
# list, mmdebstrap will notice it was changed and not populate it with
# anything. See the second paragraph in the mmdebstrap(1) which says that if you
# do NOT pass a MIRROR, then mirrors are automatically added, but if you do pass
# a MIRROR option, then you need to pass everything, or end up with a
# sources.list with only the mirror you added and nothing else.

if [ ! -z "$ADD_MIRROR" ]
then
    cat <<EOF > mirror.list
$ADD_MIRROR
deb http://deb.debian.org/debian ${SUITE} main
deb http://deb.debian.org/debian ${SUITE}-updates main
deb http://security.debian.org/debian-security ${SUITE}-security main
EOF
fi

# this will really always be "debian", but might tolerate "ubuntu" if
# you (a) change the mirror above, (b) the suite, (c) the variant,
# and, more importantly, if mmdebootstrap supports it
IMAGE=debian
# The suite is controlled by the environment variable in .gitlab-ci.yml
VARIANT=minbase
RUNTIME=podman

# No servicable parts below this line

# fetch the last update time of the mirror to create a reproducible
# tarball based on that date
ORIG_MIRROR="http://deb.debian.org/debian"
MIRROR_DATE=$(curl -s ${ORIG_MIRROR}/dists/${SUITE}/Release | /usr/bin/grep-dctrl -s Date -n '')
echo "I: mirror $ORIG_MIRROR timestamp is $MIRROR_DATE" >&2
# to have this reproducible
SOURCE_DATE_EPOCH=$(date --date="$MIRROR_DATE" +%s)
export SOURCE_DATE_EPOCH
echo "I: epoch is $SOURCE_DATE_EPOCH seconds" >&2

MODE=unshare
TAG=$CI_JOB_NAME-$VARIANT

# this creates an image similar to the official ones built by
# debuerreotype, except that instead of needing a whole set of scripts
# and hacks, we only rely on mmdebstrap.
#
# the downside is that the image is not reproducible (even if ran
# against the same mirror without change), mainly because `docker
# import` stores the import timestamp inside the image. however, the
# internal checksum itself should be reproducible.

# This is using bash's built in Parameter Expansion, specifically the
# ${parameter:+word} method, which in this case means if $ADD_MIRROR is null or
# unset, then nothing is substituted, otherwise the expansion of mirror.list is
# substituted. This particular bashism is necessary because if that variable is
# not set, then mmdebstrap does not like that. This work-around came from the
# mmdebstrap author (josch).
#
# I ran into this when I needed to add a backport mirror, and having to
# reconstruct the entire mirror list because you cannot just append to it (see
# the second paragraph in the mmdebstrap(1) about how confusing this is... with
# having to pass so many so many mirrors to the command line, I kept running
# into frustrating issues with gitlab's CI doing weird things with quoting
# (because yaml also uses quotes) and it was a lot easier to just write a
# dedicated sources.ist file and pass that to mmdebstrap instead.

mmdebstrap \
  --mode=$MODE \
  --variant=$VARIANT \
  --aptopt='Acquire::Languages "none"' \
  --dpkgopt='path-exclude=/usr/share/man/*' \
  --dpkgopt='path-exclude=/usr/share/man/man[1-9]/*' \
  --dpkgopt='path-exclude=/usr/share/locale/*' \
  --dpkgopt='path-include=/usr/share/locale/locale.alias' \
  --dpkgopt='path-exclude=/usr/share/lintian/*' \
  --dpkgopt='path-exclude=/usr/share/info/*' \
  --dpkgopt='path-exclude=/usr/share/doc/*' \
  --dpkgopt='path-include=/usr/share/doc/*/copyright' \
  --dpkgopt='path-exclude=/usr/share/omf/*' \
  --dpkgopt='path-exclude=/usr/share/help/*' \
  --dpkgopt='path-exclude=/usr/share/gnome/*' \
  --dpkgopt='path-exclude=/usr/share/examples/*' \
  --include='ca-certificates' \
  --setup-hook='cp minimize-config/dpkg.cfg.d/* "$1/etc/dpkg/dpkg.cfg.d/"' \
  --setup-hook='cp minimize-config/apt.conf.d/* "$1/etc/apt/apt.conf.d/"' \
  $SUITE \
  - \
  ${ADD_MIRROR:+mirror.list} |
    $RUNTIME import -c 'CMD ["bash"]' - $IMAGE:$TAG

$RUNTIME push $IMAGE:$TAG $CI_REGISTRY_IMAGE:$SUITE

# note that we could also have used `| tar --delete /$PATH` to exclude files
